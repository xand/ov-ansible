---
- name: Include secret vars
  include_vars:
    file: '{{ lookup("fileglob", "{{ item }}/host_vars/{{ inventory_hostname }}_secret.yml") }}'
  with_items: '{{ ansible_inventory_sources }}'

- name: Install required packages
  apt:
    name: ['vsftpd', 'libpam-pwdfile', 'apache2-utils']
    update_cache: yes

- name: Create configuration directory
  file:
    path: /etc/vsftpd
    state: directory

- name: Create per user configuration directory
  file:
    path: /etc/vsftpd/user_conf
    state: directory
    owner: root
    group: root
    mode: 0755

- name: Remove old configuration
  file:
    path: /etc/vsftpd.conf
    state: absent

- name: Remove old passwd file
  file:
    path: /etc/vsftpd/passwd
    state: absent

- name: Create empty passwd file
  file:
    path: /etc/vsftpd/passwd
    owner: root
    group: root
    mode: 0600
    state: touch

- name: Generate passwd file
  shell:
    cmd: 'echo "{{ item.name }}:$(openssl passwd -1 {{ vsftpd_host_data_secret.user_passwords[item.name] }})" >> /etc/vsftpd/passwd'
  with_items: '{{ vsftpd_host_data.users }}'

- name: Remove old denied users file
  file:
    path: /etc/vsftpd/vsftpd.denied
    state: absent

- name: Create empty denied users file
  file:
    path: /etc/vsftpd/vsftpd.denied
    owner: root
    group: root
    mode: 0644
    state: touch

- name: Generate denied users file
  lineinfile:
    path: /etc/vsftpd/vsftpd.denied
    line: '{{ item.name }}'
    state: present
    create: yes
  with_items: '{{ vsftpd_host_data.users }}'
  when: item.denied

- name: Template per user configuration
  template:
    src: templates/user_config.j2
    dest: '/etc/vsftpd/user_conf/{{ item.name }}'
    mode: 0644
    owner: root
    group: root
  with_items: '{{ vsftpd_host_data.users }}'

- name: Template main config file
  template:
    src: 'templates/vsftpd.conf.j2'
    dest: /etc/vsftpd/vsftpd.conf
    mode: 0644
    owner: root
    group: root

- name: Copy pam.d file
  copy:
    src: 'files/vsftpd.pam'
    dest: /etc/pam.d/vsftpd
    mode: 0644
    owner: root
    group: root

- name: Copy ssl cert file
  copy:
    content: '{{ vsftpd_host_data.ssl_cert }}'
    dest: /etc/vsftpd/ssl-cert.pem
    mode: 0644
    owner: root
    group: root

- name: Copy ssl key file
  copy:
    content: '{{ vsftpd_host_data_secret.ssl_key }}'
    dest: /etc/vsftpd/ssl-cert.key
    owner: root
    group: root
    mode: 0600

- name: Create systemd override directory
  file:
    path: /etc/systemd/system/vsftpd.service.d 
    state: directory
    mode: 0755
    owner: root
    group: root

- name: Create home directories
  file:
    path: '/srv/ftp/{{ item.name }}'
    state: directory
    owner: root
    group: root
  with_items: '{{ vsftpd_host_data.users }}'
  when: item.create_home

- name: Create ftp_root directories
  file:
    path: '/srv/ftp/{{ item.name }}/ftp_root'
    state: directory
    owner: '{{ item.sys_user }}'
    group: '{{ item.sys_group }}'
  with_items: '{{ vsftpd_host_data.users }}'
  when: item.create_home

- name: Copy systemd override
  copy:
    src: 'files/vsftpd.service'
    dest: /etc/systemd/system/vsftpd.service.d/override.conf
    owner: root
    group: root
    mode: 0644

- name: Restart vsftpd
  systemd:
    daemon_reload: yes
    enabled: yes
    name: vsftpd
    state: restarted









