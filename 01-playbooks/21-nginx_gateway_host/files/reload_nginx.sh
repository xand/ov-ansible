#!/bin/bash

/usr/sbin/nginx -t

if [ $? -eq "0" ]
then
        /usr/bin/echo "$(/usr/bin/date) - Reloading Nginx Configuration"
        /usr/bin/systemctl reload nginx.service
fi



