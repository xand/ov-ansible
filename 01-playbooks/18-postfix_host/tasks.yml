---
- name: Include secret vars
  include_vars:
    file: '{{ lookup("fileglob", "{{ item }}/host_vars/{{ inventory_hostname }}_secret.yml") }}'
  with_items: '{{ ansible_inventory_sources }}'

- name: Install postfix
  apt:
    name: ['postfix', 'mailutils', 'libsasl2-modules', 'sasl2-bin']
    update_cache: yes

- name: Create tls directory if needed
  file:
    path: /etc/postfix/tls
    state: directory
    owner: root
    group: root
    mode: 0755
  when: not postfix_host_data.tls_cert.default

- name: Remove tls directory if needed
  file:
    path: /etc/postfix/tls
    state: directory
    owner: root
    group: root
    mode: 0755
  when: postfix_host_data.tls_cert.default

- name: Create fullchain.pem if needed
  template:
    src: templates/fullchain.pem.j2
    dest: /etc/postfix/tls/fullchain.pem
    owner: root
    group: root
    mode: 0644
  when: not postfix_host_data.tls_cert.default

- name: Create key tls file
  copy:
    content: |
      {{ postfix_host_data_secret.tls_cert_key }}
    dest: '/etc/postfix/tls/{{ postfix_host_data.hostname }}.key'
    owner: postfix
    group: postfix
    mode: 0600
  when: not postfix_host_data.tls_cert.default

- name: Template configuration files
  template:
    src: '{{ item.src }}'
    dest: '{{ item.dst }}'
    mode: 0644
    owner: root
    group: root
  with_items:
  - { src: templates/header_check.j2, dst: /etc/postfix/header_check }
  - { src: templates/main.cf.j2, dst: /etc/postfix/main.cf }
  - { src: templates/relayhosts.j2, dst: /etc/postfix/relayhosts }
  - { src: templates/pam_smtp.j2, dst: /etc/pam.d/smtp }
  - { src: templates/login_maps.j2, dst: /etc/postfix/login_maps }
  - { src: templates/pam_users.txt.j2, dst: /tmp/pam_users.txt }
  - { src: templates/generic_maps.j2, dst: /etc/postfix/generic_maps }

- name: Template configuration files
  template:
    src: '{{ item.src }}'
    dest: '{{ item.dst }}'
    mode: 0600
  with_items:
  - { src: templates/sasl_passwd.j2, dst: /etc/postfix/sasl/sasl_passwd }

- name: Copy configuration files
  copy:
    src: '{{ item.src }}'
    dest: '{{ item.dst }}'
    owner: root
    group: root
    mode: 0644
  with_items:
  - { src: files/saslauthd, dst: /etc/default/saslauthd-postfix }
  - { src: files/master.cf, dst: /etc/postfix/master.cf }
  - { src: files/smtpd.conf, dst: /etc/postfix/sasl/smtpd.conf }

- name: Postmap needed files
  shell:
    cmd: 'postmap {{ item }}'
  with_items:
  - /etc/postfix/sasl/sasl_passwd
  - /etc/postfix/login_maps 
  - /etc/postfix/relayhosts
  - /etc/postfix/generic_maps

- name: Create required subdirectories in postfix chroot directory
  shell:
    cmd: dpkg-statoverride --add --force root sasl 710 /var/spool/postfix/var/run/saslauthd

- name: Add the user postfix to the group sasl
  user:
    name: postfix
    groups: sasl

- name: Generate pam_users database
  shell:
    cmd: 'db_load -T -t hash -f /tmp/pam_users.txt {{ postfix_host_data.users_db }}.db'

- name: Set 600 on smtpd_passwd.db
  file:
    path: /etc/postfix/sasl/smtpd_passwd.db
    owner: root
    group: root
    mode: 0600

- name: Remove pam_users file
  file:
    path: /tmp/pam_users.txt
    state: absent

- name: Restart saslauthd.service
  systemd:
    name: saslauthd
    state: restarted

- name: Restart postfix
  systemd:
    name: postfix
    state: restarted






