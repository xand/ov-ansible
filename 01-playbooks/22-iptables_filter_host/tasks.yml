---
- name: Install required packages
  apt:
    name: ['iptables', 'iptables-persistent']
    update_cache: yes

- name: Enable input/output policy (avoid lockout)
  iptables:
    chain: '{{ item.chain }}'
    policy: '{{ item.policy }}'
  with_items:
  - { chain: 'INPUT', policy: 'ACCEPT' }
  - { chain: 'OUTPUT', policy: 'ACCEPT' }

- name: Flush chains (filter)
  iptables:
    table: filter
    chain: '{{ item }}'
    flush: yes
  with_items:
  - INPUT
  - OUTPUT

- name: Flush chains (nat)
  iptables:
    table: nat
    chain: '{{ item }}'
    flush: yes
  with_items:
  - POSTROUTING

- name: Enable lo INPUT
  iptables:
    chain: INPUT
    in_interface: lo
    jump: ACCEPT

- name: Enable lo OUTPUT
  iptables:
    chain: OUTPUT
    out_interface: lo
    jump: ACCEPT

- name: Allow established and related incoming
  iptables:
    chain: INPUT
    ctstate: ['ESTABLISHED', 'RELATED']
    jump: ACCEPT

- name: Allow established outgoing
  iptables:
    chain: OUTPUT
    ctstate: ['ESTABLISHED']
    jump: ACCEPT

- name: Drop invalid incoming
  iptables:
    chain: INPUT
    ctstate: ['INVALID']
    jump: DROP

- name: Allow incoming ping request
  iptables:
    chain: INPUT
    protocol: icmp
    icmp_type: echo-request
    jump: ACCEPT
    
- name: Allow incoming ping reply
  iptables:
    chain: OUTPUT
    protocol: icmp
    icmp_type: echo-reply
    jump: ACCEPT

- name: Allow outgoing ping request
  iptables:
    chain: OUTPUT
    protocol: icmp
    icmp_type: echo-request
    jump: ACCEPT

- name: Allow incoming ping reply
  iptables:
    chain: INPUT
    protocol: icmp
    icmp_type: echo-reply
    jump: ACCEPT

- name: Add input rule
  iptables:
    chain: INPUT
    protocol: '{{ item.protocol }}'
    destination_port: '{{ item.port }}'
    source: '{{ item.address }}'
    comment: '{{ item.comment }}'
    jump: ACCEPT
  with_items: '{{ iptables_filter_host_data.input.rules }}'

- name: Add output rule
  iptables:
    chain: OUTPUT
    protocol: '{{ item.protocol }}'
    destination_port: '{{ item.port }}'
    destination: '{{ item.address }}'
    comment: '{{ item.comment }}'
    jump: ACCEPT
  with_items: '{{ iptables_filter_host_data.output.rules }}'

- name: Add MASQUERADE (nat) rules
  iptables:
    table: nat
    chain: POSTROUTING
    out_interface: '{{ item.interface }}'
    source: '{{ item.source }}'
    destination: '{{ item.destination }}'
    jump: MASQUERADE
  with_items: '{{ iptables_filter_host_data.masquerade_output_nat }}'

- name: Set default policy
  iptables:
    chain: '{{ item.chain }}'
    policy: '{{ item.policy }}'
  with_items:
  - { chain: 'INPUT', policy: '{{ iptables_filter_host_data.input.policy }}' }
  - { chain: 'OUTPUT', policy: '{{ iptables_filter_host_data.output.policy }}' }

- name: Save iptables rules
  shell:
    cmd: iptables-save > /etc/iptables/rules.v4


