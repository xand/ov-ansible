## 02-pg11-database

Installs PostgreSQL (version 11).

### Inventory

```
postgres_host_data:
  data_directory_root: /var/lib/postgresql/11
  config_directory_root: /etc/postgresql/11
  pid_directory_root: /var/run/postgresql
  clusters:
  - name: main # cluster name
    listen_address: 192.168.122.12 # ip address to listen on
    listen_port: 5432 # tcp port to listen on
    sa_user: sa_avavilin # superadmin user
    sa_user_password_change: no # if user exists should I change the password?
    sa_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
    ini_db: main_database # database name which will be created by this playbook
    ini_db_create: yes # if database does not exist, should create? (NOT IMPLEMENTED)
    ini_db_encoding: UTF8 # database encoding
    ini_db_lc_collate: en_US.UTF-8 # database collation
    ini_db_lc_ctype: en_US.UTF-8 # database ctype
    ini_db_tablespace: pg_default # tablespace name
    ini_db_conn_limit: -1 # database connection limit
    ini_db_main_user: main_user # main user for created database (change schema, select and update tables)
    ini_db_main_user_password_change: no # if user exists should I change the password?
    ini_db_main_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
    ini_db_rw_user: main_rw_user # RW user for created database (select and update only)
    ini_db_rw_user_password_change: no # if user exists should I change the password?
    ini_db_rw_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
    ini_db_ro_user: main_ro_user # RO user for created database (select only)
    ini_db_ro_user_password_change: no # if user exists should I change the password?
    ini_db_ro_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
    sa_streaming_user: sa_barman # user for streaming backups
    sa_streaming_user_password_change: no # if user exists should I change the password?
    sa_streaming_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
    streaming_user: streaming_barman # replication user (streaming backups)
    streaming_user_password_change: no # if user exists should I change the password?
    streaming_user_allowed_hosts: ['127.0.0.1/32'] # allowed ip addresses for pg_hba.conf
```

### Inventory secret

```
postgres_host_data_secret:
  cluster_user_passwords:
    main:
      sa_user_password: kkk # superadmin password
      ini_db_main_user_password: main_user_password # user's password
      ini_db_rw_user_password: main_rw_user_password # allowed ip addresses for pg_hba.conf
      ini_db_ro_user_password: main_ro_user_password # user's password
      sa_streaming_user_password: aaa # user's password
      streaming_user_password: bbb # user's password
```





