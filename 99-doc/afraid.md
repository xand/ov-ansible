### 09-afraid.org_manager

Installs necessary scripts for updating afraid.org dynamic dns.

### Inventory

```
afraid_org_manager_data:
  domains:
  - domain: dyndomain.example.com
```

### Inventory secret

```
afraid_org_manager_data_secret:
  domain_keys:
    dyndomain.example.com: xxx
```

