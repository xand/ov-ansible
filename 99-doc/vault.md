## Using Ansible Vault

[Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) is used to encrypt entire 
files with secret information, for example, password, private ssh
keys, etc. This way you may safely keep the inventories on your version control system.

Before creating encrypted file you must create a valid identity for vault. For example,
consider placing the following content into `~/.ansible.cfg`:
```
[defaults]
vault_identity_list =   identity1@~/bin/ansible-vault-password-client.py,
                        identity2@~/bin/ansible-vault-password-client.py,
                        identity3@~/bin/ansible-vault-password-client.py
```

This will create 3 identities: `identity1`, `identity2` and `identity3`. In order to get
the password for each identity in will execute the command 
`~/bin/ansible-vault-password-client.py`. This command is a simple script for getting the
password from your password manager.

In my case it contains the following:
```
#!/usr/bin/python

import os
import argparse, sys
import subprocess

ID = dict()
ID['identity1']='vault/identity1/password'
ID['identity2']='vault/identity2/password'
ID['identity3']='vault/identity3/password'

if __name__=='__main__':
    parser=argparse.ArgumentParser()

    parser.add_argument('--vault-id', help='vault id')

    args = parser.parse_args()
    v=vars(args)

    id=v.get('vault_id')
    pass_path = ID[id]

    subprocess.call(['pass', pass_path], universal_newlines=True)
```

### Create encrypted file

In order to create encrypted file use the following command:

```
$ ansible-vault create --encrypt-vault-id <label> secrets.yml
```

- `label` is used to differentiate the password used used to encrypt the file.
You may use any text here. For example: `identity2`.

- `secrets.yml` is the filename you want to create.

**IMPORTANT: You MUST use extremely strong password, min. 24 (better 32) alphanumeric 
characters.**

After performing this operation a file name `secrets.yml` will be created with the following
content:

```
$ANSIBLE_VAULT;1.2;AES256;identity2
35303365613963616631326134346631313861656631333065626139346665663230663232393364
6630323365306266656562633737653435346362613063620a316161393331383038313337326362
33343661386363343037386663633033333065393833303234353930343633633866656662633739
3932663966366432370a653966366334653132656235363338373063643162646461323332343231
6633
```

If `identity2` was used in order to create the file you will see the label on the header
line of the file.

### Edit encrypted file

In order to edit previously encrypted file use the following command:

```
$ ansible-vault edit secrets.yml
```

After executing the command you will be prompted for password in order to decrypt the contents of file.
After that your editor will pop-up with file contents where you may edit it, system editor is defined by
`$EDITOR` environment variable.

In case `vi` is too complicated you may switch to `nano` editor this way:

```
$ export EDITOR=nano
```

### Playbook execution with vault encrypted files

When executing playbook which references the content from the encrypted file you will need to provide
`--ask-vault-pass` argument. This way, ansible will ask you for the password when it needs it. 

For example: `ansible-playbook -i inventories/production --ask-vault-pass 00-setup/setup.yml`.

Also, you may consider using different ways of providing this password. See [documentation](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

