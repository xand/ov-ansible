### 25-samba_share

Playbook which configures samba shares on host.

Considerations:

- You need to open ports `139 (tcp)` and `445 (tcp)` on your firewall in order to operate Samba.
- System user `smb_sys_user` MUST exist before running that playbook.

In order to clean share that is no longer needed the following steps should be made:

- Umount `/exports/samba/share` mountpoint.
- Delete `/var/samba/share` directory.
- Delete `/exports/samba/share` directory.
- Delete system user provided in `sys_user` parameter.

An important thing to note is that each samba user is mapped to a system user, so you will need to create
all system users beforehand.

Using the `setup` playbook you may include something like that:

```
- name: smb_user_01
  groups: []
  update_password: no
  enabled: no
  generate_ssh_key: no
  create_home: no
  ssh_allowed_keys: []
  system: yes
```

There is no need to provide a user password for samba system user.

## Inventory

```
samba_share_data:
  users:
  - smb_user_01
  - smb_user_02
  - smb_user_03
  shares:
  - path: /srv/samba/it_repository
    name: it_repo
    writable: yes
    comment: IT Files Repository
    public: no
    sys_user: smb_sys_user
    valid_users:
    - smb_user_01
    - smb_user_02
```

## Inventory secret

```
samba_share_data_secret:
  user_passwords:
    smb_user_01: 1234
    smb_user_02: 1234
    smb_user_03: 1234
```



