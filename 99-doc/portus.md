## 26-portus

[Portus](http://port.us.org/) and [Docker Registry](https://docs.docker.com/registry/) host.

The system is built using [docker-compose](https://docs.docker.com/compose/), so, this playbook depends on
[01-docker_host](99-doc/docker.md).

Before deploying the system you need to generate SSL/TLS certificate/key. You may use the following command:

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj '/CN=<external_url>' -addext "subjectAltName = DNS:<external url>,DNS:<internal url>"
```

Please note the following:

- `external_url`. URL which will be used to push images from the outside. Normally you should provide machine's
domain here.
- `internal_url`. URL which will be used from within Portus docker container. Default value is `registry`,
normally you don't need to change this.

Example:

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj '/CN=metis.xand.local' -addext "subjectAltName = DNS:metis.xand.local,DNS:registry"
```

After generating the certificate and updating inventory you will need to keep `cert.pem` file inside
`/etc/docker/certs.d/<external_url>:<external_port>/ca.crt`. This step must be performed on each machine from
which you plan to push images to registry, otherwise you will get SSL certificate error. An example of
docker cert directory could be `/etc/docker/certs.d/metis.xand.local:5000/ca.crt`.

You will also need to generate secret values for `portus_secret_key_base` and `portus_password`. Please use
the following command in order to generate these values:

```
openssl rand -hex 64
```

Once the system is setup you may login via web browser in order to create admin user. You also should provide a value for External URL of docker registry, otherwise the webhooks will not work.


### Inventory

```
portus_host_data:
  base_dir: /opt/docker-apps/docker-registry
  owner: user01
  group: user01
  registry_port: 5000
  portus_port: 3000
  phpmyadmin_port: 8098
  portus_fqdn: portus.xand.es
  portus_signup_enabled: no
  portus_email_smtp_enabled: yes
  portus_email_smtp_address: smtp.gmail.com
  portus_email_smtp_port: 587
  portus_email_smtp_domain: smtp.gmail.com
  portus_email_smtp_user_name: zabbixhost.9999@gmail.com
  portus_email_smtp_authentication: login
  portus_email_smtp_enable_starttls_auto: 'true'
  portus_email_smtp_openssl_verify_mode: none
  portus_email_from: zabbixhost.9999@gmail.com
  portus_email_name: Portus Docker Registry
  portus_email_reply_to: noreply@example.com
  cert: |
    -----BEGIN CERTIFICATE-----
    MIIFPjCCAyagAwIBAgIUNQQK2dRr1LPa5UXgutxR3WVe+0kwDQYJKoZIhvcNAQEL
    ...
    -----END CERTIFICATE-----
  mariadb_user: portus
```

### Inventory secret

```
portus_host_data_secret:
  cert_key: |
    -----BEGIN PRIVATE KEY-----
    MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDXK1k1WaxJEAEl
    ...
    -----END PRIVATE KEY-----
  mariadb_root_password: xxx
  mariadb_user_password: xxx
  portus_secret_key_base: xxx
  portus_password: xxx
  portus_email_smtp_password: xxx
  portus_ldap_authentication_password: xxx
```


