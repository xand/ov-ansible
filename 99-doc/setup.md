
## 07-setup

Performs more fine-grained setup tasks on the machine.
For instance:

- Sets the timezone
- Configures globar environment variables
- Configures additional networks
- Sets authorized keys for users (*TODO: Explain the process*)
- Creates new users.
- Configures email sending using msmtp.

Network configuration:

- If `setup_host_data.network_configure` is `no` than no configuration is performed. Just the contents of `setup_host_data.network_content` is copied inside `/etc/network/interfaces`. Otherwise, full configuration is performed.
- If `setup_host_data.resolv_configure` is `no` than no configuration is performed. Just the contents of `setup_host_data.resolv_content` is copied inside `/etc/resolv.conf`. Otherwise, full configuration is performed.

**Please note: you probably will need to reboot your machine after applying this playbook.**

### msmtp

In order to test the mail configuration you may issue the following command:
```
echo "Test" | mail -s "Test to root user" root
```

### Inventory

```
setup_host_data:
  timezone: Europe/Madrid
  root_ssh_keys:
  - ssh-rsa ...
  network_configure: yes
  network_content: |
    # interfaces from ansible
    source /etc/network/interfaces.d/*

    # The loopback network interface
    auto lo
    iface lo inet loopback

    auto enp1s0
    iface enp1s0 inet static
        address 192.168.122.13
        netmask 255.255.255.0
        gateway 192.168.122.1
  resolv_configure: yes
  resolv_content: |
    # resolv.conf from ansible
    nameserver 192.168.122.1
  networks: 
  - mac: '56:95:70:fd:d3:b7'
    dhcp: false # if this is true, the rest is ignored
    address: 172.16.2.100
    netmask: 255.255.255.0
    gateway: 172.16.2.3
    dns: ['172.16.2.114']
    pointopoint:
    mtu: 1400
    routes:
    - net: 10.90.0.0/16
      via: 172.16.2.101
      dev: '56:95:70:fd:d3:b7'
  users:
  - name: jenkins_luna
    groups: []
    update_password: false 
    enabled: true
    generate_ssh_key: no
    create_home: no
    system: no
    ssh_allowed_keys:
    - ssh-rsa ...
  msmtp:
    configure: yes
    port: 587
    server: smtp.gmail.com
    from: email@example.com
    user: email@example.com
    default_alias: email03@example.com
    aliases:
    - local: root
      remote:
      - email01@example.com
      - email02@example.com
  environment:
  - name: http_proxy
    value: 'http://service-pro-01.corp.overlap.net:3128'
  - name: https_proxy
    value: 'http://service-pro-01.corp.overlap.net:3128'
  - name: HTTP_PROXY
    value: 'http://service-pro-01.corp.overlap.net:3128'
  - name: HTTPS_PROXY
    value: 'http://service-pro-01.corp.overlap.net:3128'
  - name: NO_PROXY
    value: '.example.local,localhost'
  ntp_servers:
  - service-pro-01.corp.overlap.net
  ssh:
    listen_address: 172.16.2.100
```

### Inventory secrets

```
setup_host_data_secret:
  user_passwords:
    user_1: secret_user1_password
  msmtp:
    password: secret_smtp_password
```








