### 06-vsftpd_pod_host

Installs vsftpd (with SSL) on the host.

**IMPORTANT** User which is mapped by `guest_username` MUST have its home directory created (check `/etc/passwd`). Also it is mandatory to create the directory provided in `local_root`.
Otherwise you will get strange connection errors.
Example: `Fatal error: gnutls_record_recv: An unexpected TLS packet was received.`

In order to connect to the server please see the following screenshot (Filezilla).

![vsftpd Filezilla](img/filezilla_capture.png "VSFTPd connection with Filezilla")

### Inventory

```
vsftpd_host_data:
  ssl_cert: |
    -----BEGIN CERTIFICATE-----
    MIIC0DCCAbigAwIBAgIUCQLfb3xMRFv9++GYOmbDf9c4kWMwDQYJKoZIhvcNAQEL
    ...
    -----END CERTIFICATE-----
  pasv_address: 192.168.122.13
  pasv_min_port: 12000
  pasv_max_port: 12100
  users:
    - name: user_01
      denied: no
      sys_user: user_01_sys
      sys_group: user_01_sys
      local_root: /opt/root_dir_for_user_01
      create_home: yes
    - name: user_02
      denied: no
      sys_user: user_02_sys
      sys_group: user_02_sys
      local_root: /opt/root_dir_dir_user_02
      create_home: yes
```

Notes:

- `pasv_address` Passive server address. Provide externally accessible IP here. Please note, that you cannot have the same FTP listen on both internal/external networks. Only one of them is allowed.

- `sys_user` MUST exist in the moment of executing this playbook.

- `local_root` MUST exist in the moment of executing this playbook. Also, directory provided in this parameter
MUST not be writeable by user. Otherwise an error will occur when trying to connect.

- `create_home` will create `/srv/ftp/<ftp_user_name>/ftp_root` directory.

### Inventory secrets

```
vsftpd_host_data_secret:
  user_passwords:
    user_01: user_01_secret_password
    user_02: user_02_secret_password
  ssl_key: |
    -----BEGIN PRIVATE KEY-----
    MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDQVVRO58OjqTf2
    ...
    -----END PRIVATE KEY-----
```








