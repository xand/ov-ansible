## Running ansible on Windows 10

In order to run ansible on Windows you will need to install WSL (Windows Subsystem for Linux). You may
find a lot of information online about how to achieve this. For example, Microsoft provides official
[manual](https://docs.microsoft.com/es-es/windows/wsl/install-win10). The use of Debian GNU/Linux is strongly
advised.

Once installed proceed as you would do on any Linux machine.

```
# apt-get update
# apt-get install ansible
```

Enjoy!



