## 01-docker_host

Installs latest [docker](https://www.docker.com/) (from official repositories) to host.

### Inventory

```
docker_host_data:
  users: ['docker_user']
  environment:
  - name: HTTP_PROXY
    value: http://service-pro-01.corp.overlap.net:3128
  - name: HTTPS_PROXY
    value: http://service-pro-01.corp.overlap.net:3128
  - name: NO_PROXY
    value: '.xand.local,localhost'
  mtu: 1500
  dns:
  - '172.16.2.114'
  listen_sockets:
  - 'tcp://127.0.0.1:2375'
  - 'unix:///var/run/docker.sock'
  registries:
    certificates:
    - host: reg1.example.com:5000
      certificate: |
        -----BEGIN CERTIFICATE-----
        MIIFPjCCAyagAwIBAgIUbQ+44wvQ7by+mnLBmSRwZJYRF2YwDQYJKoZIhvcNAQEL
        ...
        -----END CERTIFICATE-----
    - host: reg2.example.com:5000
      certificate: |
        -----BEGIN CERTIFICATE-----
        MIIFPjCCAyagAwIBAgIUcVTN+5r+5pHytTmPvJYmUdmpIeEwDQYJKoZIhvcNAQEL
        ...
        -----END CERTIFICATE-----
```

Notes:

- `mtu` value is optional, if not present the default value is 1500.

- `environment` values are dedicated to docker daemon.


### Inventory secrets

Actually the playbook does not have secret part.



