## 20-zabbix_host

Installs and configures zabbix platform on the host. Please not that this playbook makes use of
`ansible_local.zabbix_installed` fact. If the value is `true` fresh installation (initial database schema
import) is not performed.

### Considerations

- Unless you're using reverse proxy (like nginx), you must open port (input) for web frontend (default: 8888).

- You must open port for zabbix external agents (default: 10050).

- In order to use email alert you must open output port for SMTP protocol, usually TCP/587.

- After installation point your browser to `http://<ip_address>:<port>/zabbix/` in order to finish the
installation.

- Default username is `Admin` and password is `zabbix`. **You must change it ASAP**, otherwise, if your zabbix
frontend is reachable from the outside it will be hacked.

- This playbook does not perform the installation of MariaDB (not anymore), so, the database should exist
before running the script.

### Email alerts configuration

In order to send alert emails you should configure it. Before configuration you should have at hand your SMTP
configuration detail:

- SMTP host.
- SMTP server port.
- SMTP email.
- SMTP connection security.
- SMTP authentication.

Once you have all data in hand you may navigate to `Administration > Media types > Email (HTML)` in order
to perform the configuration. Please note that **you should disable all media types that are not in use**.

Below you may see an example configuration using gmail server:

![Zabbix gmail configuration](img/zabbix_email_config.png)


### Using PSK encryption

Sometimes, when not in private networks you may need to use encryption for zabbix communications.
Please consider that network discovery of hosts may not work when the communication is encrypted.

In order to generate PSK key you may use the following command:

```
openssl rand -hex 32
```

After executing this command you will get a string like 
`7235612eb435adebec98d9cc70877529f3ce291a4c00f9a9d434622cc2311b89`. This string must be kept secret. When
configuring [zabbix proxy](zabbix-proxy.md) you will need to provide this string as 
`zabbix_proxy_host_data_secret.psk` parameter.

### Modules

#### check_ssl
Periodically validates SSL/TLS certificates. 
[More info](https://github.com/selivan/https-ssl-cert-check-zabbix).

In order to check that the check works you may use the following:

```
$ zabbix_get -s 127.0.0.1 -p 10050 -k "ssl_cert_check_expire[<server_ip>,<server_port>,<domain>,<timeout>]"

$ zabbix_get -s 127.0.0.1 -p 10050 -k "ssl_cert_check_valid[<server_ip>,<server_port>,<domain>,<timeout>]"
```

**IMPORTANT:** When issuing `zabbix_get` command should use the ip address (127.0.0.1) instead of `localhost`,
otherwise you may get `zabbix_get [27843]: Check access restrictions in Zabbix agent configuration`.

**Installation instructions:**

- Once the server is fully configured you should import the template for SSL checks. You may perform this 
operation from `Configuration > Import` menu.

- After template import you may create a discovery rule for a template. This option is accessible from 
`Configuration > Templates > Template Web SSL Certificate Monitor > Discovery > Create discovery rule`. The configuration should be like shown on the screenshot:

![Zabbix SSL discovery rule](img/zabbix_ssl_discovery_rule.png)

- Once the import is performed you should apply the template for the host. This option is accesible from
`Configuration > Hosts > Zabbix host > Templates`. Once there you should link a new template to the host.
Just select `Template SSL_TLS certificates check` and link it to the host.

![Zabbix SSL link template](img/zabbix_link_template.png)

### Inventory

```
zabbix_host_data:
  timezone: 'Europe/Madrid'
  discoverers: 5
  modules:
    check_ssl:
      enabled: yes
      data:
      - server_ip: 8.8.8.8
        domain: www.example.com
        port: 443
        timeout: 5
      - server_ip: 8.8.4.4
        domain: www.example.com
        port: 443
        timeout: 5
  web:
    server_port: 8888
  mariadb:
    user: zabbix
    db_name: zabbix
    host: localhost
```

### Inventory secret

```
zabbix_host_data_secret:
  mariadb_password: mariadb_secret_password
```



