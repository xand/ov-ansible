## 04-luks_pod_host

Prepares the host for hosting secured LUKS containers.

### Inventory

```
luks_pod_host_data:
  mountpoint_root_path: /mnt/luks_pods
  containers_root_path: /srv/luks_pods
  pods:
    - name: P64-1887
      loop_device: /dev/loop0
      size: 100M
```

### Inventory secret

```
luks_pod_host_data:
  device_passphrases:
    P64-1887: 1234
```




