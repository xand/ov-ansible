## 16-zabbix_proxy_host

Proxy configurarion allows PSK encryption. Use the following command to generate the key:

```
openssl rand -hex 32
```

Don't forget to keep it secret.

### Inventory

```
zabbix_proxy_host_data:
  allowed_servers: '8.8.8.8,8.8.4.4'
  hostname: proxy.example.com
  use_psk: yes
  psk_identity: 'PSK ID 01'
```

### Inventory secret

```
zabbix_proxy_host_data_secret:
  psk: e336d1f8ce6d5fd79c413b2ec03e91c7105b53c40cc203c562123f0760ea9a1b 
```



