## 11-keycloak_10_host

Install Keycloak 10 on the host. Before running this playbook you MUST install Java on the host system.

In order to save space (and being able to upload keycloak installation file to git) we use xz compression.
You may use the following procedure to create distribution file (experimentally I achieve the best compression
this way, but it will take some time):

1. Create directory `keycloak-10.0.0` somewhere and put the installation files. It should look something
like that:

```
keycloak
├── bin
├── docs
├── domain
├── jboss-modules.jar
├── LICENSE.txt
├── modules
├── standalone
├── themes
├── version.txt
└── welcome-content

7 directories, 3 files
```

2. Execute the following command:

```
tar -cvf - keycloak/ | xz --threads=0 -9e -c - > keycloak.tar.xz
```

That's it, you know have your own keycloak distribution.

In order to decompress the file you should use the following command:

```
tar --xz -xf keycloak.tar.xz
```

After that your files will be inside `keycloak` directory.

### Inventory

```
keycloak_10_host_data:
  install_path: /opt/software/keycloak-10.0.0
  admin_user: admin
  keycloak_system_user: keycloak
  keycloak_system_group: keycloak
  listen_port_offset: 1010 # this offset will make a sum with 8080 default port
  management_port: 11000
```

### Inventory secret

```
keycloak_10_host_data_secret:
  admin_password: admin
```





