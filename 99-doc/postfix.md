### 18-postfix_host

In order to test the configuration with SASL authentication enabled you may use the following approach:

1. Generate username/password and base64-encode it:
```
echo -n "user1" | base64

dXNlcjE=
cGFzczE=

echo -n "user2" | base64
dXNlcjI=
cGFzczI=
```

2. Connect to the server using the following command and auth yourself:
```
openssl s_client -connect 192.168.122.13:587 -starttls smtp
AUTH LOGIN
<base64 username>
<base64 password>
```

3. Issue the commands to postfix server:
```
mail from: <user1@example.com>
rcpt to: <xand@xand.es>
data
From: "Hello world" <user1@example.com>
To: <xand@xand.es>
Subject: Test message
Test message for testing postfix configuration.

.
QUIT
```

**How it works**

Each user authenticates via pam module (`pam_userdb`). Please note that the domain is never part of username.
For example: if you're sending the email from `user1@example.com` username should be `user1`.

You should also note that `From` field must contain the internal email address, 
for example `user1@example.com`. Otherwise, message will be rejected.

After pam module allowed you in the following configuration applies:

1. Envelope `From` field is matched against `login_maps`. If match not found than the message is rejected.

2. Based on envelope `From` field a relay is choosen from `relayhosts` file. 

3. Based on envelope `From` field user credentials for relay are choosed from `sasl/sasl_passwd` file.

4. `From` header (not the envelope) is replaced according to `header_check` file.
If user provided in the `From` header is not a permitted one, the message is rejected.

5. Finally the relaying occurs, as the last point before leaving the server message is processed according
to `generic_maps` file. At this time the real email address is set instead of internal one.

**Note about TLS certificates**

When a client connects to Postfix server a check of TLS certificate normally occurs. In order to get a valid
configuration (without complaining about self-signed certificates) you should perform the following steps:

1. Generate self-signed CA certificate:

`openssl genrsa -aes128 -out ca.key 4096` (you will be asked for password at this point)

`openssl req -x509 -new -nodes -key ca.key -sha256 -days 7300 -out ca.pem -subj "/C=ES/ST=STATE/L=LOCALITY/O=ORGANIZATION/CN=CA-CN"`

2. Generate CSR for server installation (at this point you will need to provide real domain (internal or external):

`openssl req -new -newkey rsa:2048 -nodes -keyout postfix.key -out postfix.csr -subj "/C=ES/ST=STATE/L=LOCALITY/O=ORGANIZATION/CN=example.com"` 

3. Sign CSR with CA generated at step 1:
`openssl x509 -req -in postfix.csr -CA ca.pem -CAkey ca.key -CAcreateserial -out postfix.crt -days 7300 -sha256`

After that you will get 3 important files whose content should be transferred to inventory file:
- `ca.pem`. Goes into `postfix_host_data.tls_cert.ca_cert`.
- `postfix.crt`. Goes into `postfix_host_data.tls_cert.cert`.
- `postfix.key`. Goes into `postfix_host_data_secret.tls_cert_key`.

### Inventory

```
postfix_host_data:
  mynetworks: 192.168.122.0/24
  hostname: example.com
  users_db: /etc/postfix/sasl/smtpd_passwd
  tls_cert:
    default: no
    cert: |
      -----BEGIN CERTIFICATE-----
      MIIESDCCAjACFDZcnyewmlJAi6+4U3WBQ5Bsw0tsMA0GCSqGSIb3DQEBCwUAMFYx
      ...
      -----END CERTIFICATE-----
    ca_cert: |
      -----BEGIN CERTIFICATE-----
      MIIFjTCCA3WgAwIBAgIUCXS/h0+RRNTkcD6JqghRA5pbM0wwDQYJKoZIhvcNAQEL
      ...
      -----END CERTIFICATE-----
  users:
  - username: user1
    relay_username: <username>
    relay_host: smtp.gmail.com
    relay_port: 587
    relay_rewrite_name: 'Sistemas internos'
    relay_rewrite_address: <email_address>
  - username: user2
    relay_username: <username>
    relay_host: smtp.gmail.com
    relay_port: 587
    relay_rewrite_name: 'Developer'
    relay_rewrite_address: <email_address>
```

### Inventory secret

```
postfix_host_data_secret:
  user_passwords:
    user1: user1_secret
  relay_user_passwords:
    user1: user1_relay_secret
  tls_cert_key: |
    -----BEGIN RSA PRIVATE KEY-----
    MIIEpAIBAAKCAQEAntEF+9voqMZjjpoCvVCvv7rGD2QtIdxA7Upt0Xck4gSas92I
    ...
    -----END RSA PRIVATE KEY-----
```





