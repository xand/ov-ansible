## 12-barman

Installs [barman](https://www.pgbarman.org/) on the host.

### Inventory

```
barman_host_data:
  services:
  - name: cluster_name
    description: cluster_description
    conn_pg_host: cluster_host
    conn_pg_port: 5433
    conn_pg_user: sa_barman
    conn_pg_ddbb: postgres
    st_conn_pg_host: cluster_host
    st_conn_pg_port: 5433
    st_conn_pg_user: streaming_barman
    st_conn_pg_ddbb: replication
    slot_name: arce_barman
    full_backup_schedule: '*-*-* 14:25:00'
```

### Inventory secrets

```
barman_host_data_secret:
  cluster_name:
    conn_pg_password: conn_pg_user_passwd
    st_conn_pg_password: st_conn_pg_user_passwd
```




