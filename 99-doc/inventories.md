## Managing inventories

The following applies for managing inventories:

- Your inventory should be stored at the root of the project. All relative directories will be built
according to this assumption.

- You may have multiple inventories under the same version control. For example: `prod`, `staging`, etc.

- Inventories are stored on version control system. Sensible data is encrypted using `ansible-vault`.

- Each inventory has `hosts` file where all machines are listed. Also you should define at least 2 additional
parameters for each machine: `ansible_ssh_host` and `ansible_ssh_user`.

- Each inventory has `host_vars` directory. This directory contains two files for each machines defined 
in `hosts` file: `<machine_name>.yml` and `<machine_name>_secret.yml`. The first file contains variables
for the host in clear text (world-readable). The second one contains the secret variables and it MUST be
encrypted using `ansible-vault`.

Consider the following repository:

```
.
├── production
│   ├── hosts
│   └── host_vars
│       ├── debian_01.yml
│       ├── debian_02.yml
│       └── debian_03.yml
└── setup
    ├── hosts
    └── host_vars
        ├── my_host_01_secret.yml
        ├── my_host_01.yml
        ├── my_host_02_secret.yml
        └── my_host_02.yml

```

When executing, each playbook will include secret variables with the following statement:

```
    - name: Include secret vars
      include_vars:
        file: '{{ lookup("fileglob", "{{ item }}/host_vars/{{ inventory_hostname }}_secret.yml") }}'
      with_items: '{{ ansible_inventory_sources }}'
```





