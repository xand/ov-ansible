## 19-openvpn_host

Openvpn host.

**IMPORTANT**: You must execute this playbook after iptables playbook.

If you decide to use `redirect_gateway` option for client you will also need to add MASQUERADE rules to iptables configuration. You may use something like that:

```
iptables -t nat -I POSTROUTING -o eth0 -s 10.8.0.0/24 -j MASQUERADE
```

### Server certificate

For certificate generation, please use `easy-rsa.zip` provided in `97-utils` directory.

1. Unzip `easy-rsa.zip` somewhere.

2. Create `vars` file inside `easy-rsa` directory:

```
cat << EOF > ./vars
set_var EASYRSA                 "\$PWD"
set_var EASYRSA_PKI             "\$EASYRSA/pki"
set_var EASYRSA_DN              "cn_only"
set_var EASYRSA_REQ_COUNTRY     "SPAIN"
set_var EASYRSA_REQ_PROVINCE    "Madrid"
set_var EASYRSA_REQ_CITY        "Madrid"
set_var EASYRSA_REQ_ORG         "xand.es CERTIFICATE AUTHORITY"
set_var EASYRSA_REQ_EMAIL	      "xand@xand.es"
set_var EASYRSA_REQ_OU          "xand.es EASY CA"
set_var EASYRSA_KEY_SIZE        2048
set_var EASYRSA_ALGO            rsa
set_var EASYRSA_CA_EXPIRE	      7500
set_var EASYRSA_CERT_EXPIRE     365
set_var EASYRSA_NS_SUPPORT	    "no"
set_var EASYRSA_NS_COMMENT	    "xand.es CERTIFICATE AUTHORITY"
set_var EASYRSA_EXT_DIR         "\$EASYRSA/x509-types"
set_var EASYRSA_SSL_CONF        "\$EASYRSA/openssl-easyrsa.cnf"
set_var EASYRSA_DIGEST          "sha256"
EOF
```

This file will be used for all the defaults in the following CRT generation.

3. Perform init pki:

```
./easyrsa init-pki
```

4. Generate CA (you will need to provide passphrase and a CN attribute for CA):

```
./easyrsa build-ca
```

After this step you will be able to find your ca crt file at the following location `pki/ca.crt`.

5. Generate server certificate with no password and sign it:

```
./easyrsa gen-req <server_cn> nopass
./easyrsa sign-req server <server_cn>
```

After these steps you will need two files:
- `pki/issued/<server_cn>.crt` (certificate)
- `pki/private/<server_cn>.key` (private key)

6. Generate strong Diffie-Hellman key to use for the key exchange:

```
./easyrsa gen-dh
```

You will find the result file in `pki/dh.pem`.

### Client certificates

In order to configure client certificates.

1. Generate client certificate request:

```
./easyrsa gen-req <client_cn> <nopass>
```

If you don't want to protect the key file with password (while you definitely should), just provide `nopass` option.

2. Sign client certificate request:

```
./easyrsa sign-req client <client_cn>
```

You will be asked for CA's password.

In order to generate your `client.ovpn` configuration file you will need the following files:
- `pki/ca.crt`
- `pki/issued/<client_cn>.crt`
- `pki/private/<client_cn>.key`

### Client configuration

#### For Android:

```
client
dev tun
proto udp
remote 192.168.122.13 1194
cipher AES-256-CBC
auth SHA512
auth-nocache
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
script-security 2
verb 3

<ca>
-----BEGIN CERTIFICATE-----
MIIDVzCCAj+gAwIBAgIUNWhx68rv6yhHxWuJskILqqtAGOcwDQYJKoZIhvcNAQEL
...
-----END CERTIFICATE-----
</ca>

<cert>
-----BEGIN CERTIFICATE-----
MIIDiTCCAnGgAwIBAgIQCB5ENNPk1wgBldOlfWJDTzANBgkqhkiG9w0BAQsFADAa
...
-----END CERTIFICATE-----
</cert>

<key>
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDL1aNIuIQxPnNS
...
-----END PRIVATE KEY-----
</key>
```

**Use Android keystore for cert/key PKCS#12**

When using this approach you will be able to delegate all the security part to native Android mechanisms. For instance, you will not need to enter password each time you connect while keeping the same level of security.

1. Convert cert/key to PKCS#12 format using the following command (you will be asked for key password and also for export password):

```
openssl pkcs12 -export -in pki/issued/<client_cn>.crt -inkey pki/private/<client_cn>.key -certfile pki/ca.crt -name <client_cn> -out <client_cn>.p12
```

2. Once generated you will need to transfer it somehow to mobile device (Email/WhatsApp/whatever). Once on the device just open it and answer YES when prompted for import (you will need to proved the export password).

3. After the import you may use the original .ovpn file removing `<cert>`, `<key>` and `<ca>` elements.

4. When connecting for the first time you will need to select the certificate when prompted.

#### For Windows machine:

```
client
dev tun
proto udp
remote 192.168.122.13 1194
cipher AES-256-CBC
auth SHA512
auth-nocache
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
script-security 2
verb 3

<ca>
-----BEGIN CERTIFICATE-----
MIIDVzCCAj+gAwIBAgIUNWhx68rv6yhHxWuJskILqqtAGOcwDQYJKoZIhvcNAQEL
...
-----END CERTIFICATE-----
</ca>

<cert>
-----BEGIN CERTIFICATE-----
MIIDiTCCAnGgAwIBAgIQCB5ENNPk1wgBldOlfWJDTzANBgkqhkiG9w0BAQsFADAa
...
-----END CERTIFICATE-----
</cert>

<key>
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDL1aNIuIQxPnNS
...
-----END PRIVATE KEY-----
</key>
```

#### For linux machine:

```
client
daemon
dev tun
proto udp
remote 192.168.122.13 1194
cipher AES-256-CBC
auth SHA512
auth-nocache
tls-version-min 1.2
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256:TLS-DHE-RSA-WITH-AES-128-GCM-SHA256:TLS-DHE-RSA-WITH-AES-128-CBC-SHA256
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
user openvpn
group network
writepid /run/openvpn-client/openvpn_test.pid
script-security 2
setenv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
up /etc/openvpn/scripts/update-systemd-resolved
up-restart
down /etc/openvpn/scripts/update-systemd-resolved
down-pre
verb 3

<ca>
-----BEGIN CERTIFICATE-----
MIIDVzCCAj+gAwIBAgIUNWhx68rv6yhHxWuJskILqqtAGOcwDQYJKoZIhvcNAQEL
...
-----END CERTIFICATE-----
</ca>

<cert>
-----BEGIN CERTIFICATE-----
MIIDiTCCAnGgAwIBAgIQCB5ENNPk1wgBldOlfWJDTzANBgkqhkiG9w0BAQsFADAa
...
-----END CERTIFICATE-----
</cert>

<key>
-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDL1aNIuIQxPnNS
...
-----END PRIVATE KEY-----
</key>
```


### Inventory

```
openvpn_host_data:
  ca_cert: | # contents of pki/ca.crt
    -----BEGIN CERTIFICATE-----
    MIIDVzCCAj+gAwIBAgIUNWhx68rv6yhHxWuJskILqqtAGOcwDQYJKoZIhvcNAQEL
    ...
    -----END CERTIFICATE-----
  server_cert: | # contents of pki/issued/<server_cn>.crt
    -----BEGIN CERTIFICATE-----
    MIIDiTCCAnGgAwIBAgIQCB5ENNPk1wgBldOlfWJDTzANBgkqhkiG9w0BAQsFADAa
    ...
    -----END CERTIFICATE-----
  dh: | # contents of pki/dh.pem
    -----BEGIN DH PARAMETERS-----
    MIIBCAKCAQEA6tS7dZG68izw1FzxmDocfK+FpiTEvNh11dyqZTxhFTYjEbyGWSVs
    ...
    -----END DH PARAMETERS-----

  server_name: my_server
  local_address: 192.168.122.13
  listen_port: 1194
  server_network: 10.8.0.0
  server_netmask: 255.255.255.0
  push_dns:
  - 8.8.8.8
  - 8.8.4.4
  clients:
  - cn: client_cn
    # see: https://gitlab.com/xand/ov-ansible/-/issues/107
    redirect_gateway: no #PLEASE DO NOT USE THIS OPTION AT THE MOMENT
    routes:
    - address: 172.16.10.0
      netmask: 255.255.255.255
      gateway: 10.8.0.1
    dns:
    - 1.1.1.1
    - 2.2.2.2
    domains:
    - corp.example.com

```

### Inventory secret

```
openvpn_host_data_secret:
  server_key: | # contents of pki/private/<server_cn>.key
    -----BEGIN PRIVATE KEY-----
    MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDL1aNIuIQxPnNS
    ...
    -----END PRIVATE KEY-----
```

### APPENDIX (linux client configuration)

`/etc/openvpn/scripts/update-systemd-resolved` contents:

```
#!/usr/bin/env bash
#
# OpenVPN helper to add DHCP information into systemd-resolved via DBus.
# Copyright (C) 2016, Jonathan Wright <jon@than.io>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This script will parse DHCP options set via OpenVPN (dhcp-option) to update
# systemd-resolved directly via DBus, instead of updating /etc/resolv.conf. To
# install, set as the 'up' and 'down' script in your OpenVPN configuration file
# or via the command-line arguments, alongside setting the 'down-pre' option to
# run the 'down' script before the device is closed. For example:
#   up /etc/openvpn/scripts/update-systemd-resolved
#   down /etc/openvpn/scripts/update-systemd-resolved
#   down-pre

# Define what needs to be called via DBus
DBUS_DEST="org.freedesktop.resolve1"
DBUS_NODE="/org/freedesktop/resolve1"

SCRIPT_NAME="${BASH_SOURCE[0]##*/}"

log() {
  logger -s -t "$SCRIPT_NAME" "$@"
}

for level in emerg err warning info debug; do
  printf -v functext -- '%s() { log -p user.%s -- "$@" ; }' "$level" "$level"
  eval "$functext"
done

usage() {
  err "${1:?${1}. }. Usage: ${SCRIPT_NAME} up|down device_name."
}

busctl_call() {
  # Preserve busctl's exit status
  busctl call "$DBUS_DEST" "$DBUS_NODE" "${DBUS_DEST}.Manager" "$@" || {
    local -i status=$?
    emerg "'busctl' exited with status $status"
    return $status
  }
}

get_link_info() {
  dev="$1"
  shift

  link=''
  link="$(ip link show dev "$dev")" || return $?

  echo "$dev" "${link%%:*}"
}

dhcp_settings() {
  for foreign_option in "${!foreign_option_@}"; do
    foreign_option_value="${!foreign_option}"

    [[ "$foreign_option_value" == *dhcp-option* ]] \
      && echo "${foreign_option_value#dhcp-option }"
  done
}

up() {
  local link="$1"
  shift
  local if_index="$1"
  shift

  info "Link '$link' coming up"

  # Preset values for processing -- will be altered in the various process_*
  # functions.
  local -a dns_servers=() dns_domain=() dns_search=() dns_routed=()
  local -i dns_server_count=0 dns_domain_count=0 dns_search_count=0 dns_routed_count=0
  local dns_sec=""

  while read -r setting; do
    setting_type="${setting%% *}"
    setting_value="${setting#* }"

    process_setting_function="${setting_type,,}"
    process_setting_function="process_${process_setting_function//-/_}"

    if declare -f "$process_setting_function" &>/dev/null; then
      "$process_setting_function" "$setting_value" || return $?
    else
      warning "Not a recognized DHCP setting: '${setting}'"
    fi
  done < <(dhcp_settings)

  if [[ "${#dns_servers[*]}" -gt 0 ]]; then
    busctl_params=("$if_index" "$dns_server_count" "${dns_servers[@]}")
    info "SetLinkDNS(${busctl_params[*]})"
    busctl_call SetLinkDNS 'ia(iay)' "${busctl_params[@]}" || return $?
  fi

  if [[ "${#dns_domain[*]}" -gt 0 \
     || "${#dns_search[*]}" -gt 0 \
     || "${#dns_routed[*]}" -gt 0 ]]; then
    dns_count=$((dns_domain_count+dns_search_count+dns_routed_count))
    busctl_params=("$if_index" "$dns_count")
    if [[ "${#dns_domain[*]}" -gt 0 ]]; then
      busctl_params+=("${dns_domain[@]}")
    fi
    if [[ "${#dns_search[*]}" -gt 0 ]]; then
      busctl_params+=("${dns_search[@]}")
    fi
    if [[ "${#dns_routed[*]}" -gt 0 ]]; then
      busctl_params+=("${dns_routed[@]}")
    fi
    info "SetLinkDomains(${busctl_params[*]})"
    busctl_call SetLinkDomains 'ia(sb)' "${busctl_params[@]}" || return $?
  fi

  if [[ -n "${dns_sec}" ]]; then
    if [[ "${dns_sec}" == "default" ]]; then
      # We need to provide an empty string to use the default settings
      info "SetLinkDNSSEC($if_index '')"
      busctl_call SetLinkDNSSEC 'is' "$if_index" "" || return $?
    else
      info "SetLinkDNSSEC($if_index ${dns_sec})"
      busctl_call SetLinkDNSSEC 'is' "$if_index" "${dns_sec}" || return $?
    fi
  fi
}

down() {
  local link="$1"
  shift
  local if_index="$1"
  shift

  info "Link '$link' going down"
  if [[ "$(whoami 2>/dev/null)" != "root" ]]; then
    # Cleanly handle the privilege dropped case by not calling RevertLink
    info "Privileges dropped in the client: Cannot call RevertLink."
  else
    busctl_call RevertLink i "$if_index"
  fi
}

process_dns() {
  address="$1"
  shift

  if looks_like_ipv6 "$address"; then
    process_dns_ipv6 "$address" || return $?
  elif looks_like_ipv4 "$address"; then
    process_dns_ipv4 "$address" || return $?
  else
    err "Not a valid IPv6 or IPv4 address: '$address'"
    return 1
  fi
}

process_dns6() {
  process_dns $1
}

looks_like_ipv4() {
  [[ -n "$1" ]] && {
    local dots="${1//[^.]}"
    (( ${#dots} == 3 ))
  }
}

looks_like_ipv6() {
  [[ -n "$1" ]] && {
    local colons="${1//[^:]}"
    (( ${#colons} >= 2 ))
  }
}

process_dns_ipv4() {
  local address="$1"
  shift

  info "Adding IPv4 DNS Server ${address}"
  (( dns_server_count += 1 ))
  dns_servers+=(2 4 ${address//./ })
}

# Enforces RFC 5952:
#   1. Don't shorten a single 0 field to '::'
#   2. Only longest run of zeros should be compressed
#   3. If there are multiple longest runs, the leftmost should be compressed
#   4. Address must be maximally compressed, so no all-zero runs next to '::'
#
# ...
#
# Thank goodness we don't have to handle port numbers, though :)
parse_ipv6() {
  local raw_address="$1"

  log_invalid_ipv6() {
    local message="'$raw_address' is not a valid IPv6 address"
    emerg "${message}: $*"
  }

  trap -- 'unset -f log_invalid_ipv6' RETURN

  if [[ "$raw_address" == *::*::* ]]; then
    log_invalid_ipv6 "address cannot contain more than one '::'"
    return 1
  elif [[ "$raw_address" =~ :0+:: ]] || [[ "$raw_address" =~ ::0+: ]]; then
    log_invalid_ipv6 "address contains a 0-group adjacent to '::' and is not maximally shortened"
    return 1
  fi

  local -i length=8
  local -a raw_segments=()

  IFS=$':' read -r -a raw_segments <<<"$raw_address"

  local -i raw_length="${#raw_segments[@]}"

  if (( raw_length > length )); then
    log_invalid_ipv6 "expected ${length} segments, got ${raw_length}"
    return 1
  fi

  # Store zero-runs keyed to their sizes, storing all non-zero segments prefixed
  # with a token marking them as such.
  local nonzero_prefix=$'!'
  local -i zero_run_i=0 compressed_i=0
  local -a tokenized_segments=()
  local decimal_segment='' next_decimal_segment=''

  for (( i = 0 ; i < raw_length ; i++ )); do
    raw_segment="${raw_segments[i]}"

    printf -v decimal_segment -- '%d' "0x${raw_segment:-0}"

    # We're in the compressed group.  The length of this run should be
    # enough to bring the total number of segments to 8.
    if [[ -z "$raw_segment" ]]; then
      (( compressed_i = zero_run_i ))

      # `+ 1' because the length of the current segment is counted in
      # `raw_length'.
      (( tokenized_segments[zero_run_i] = ((length - raw_length) + 1) ))

      # If we have an address like `::1', skip processing the next group to
      # avoid double-counting the zero-run, and increment the number of
      # 0-groups to add since the second empty group is counted in
      # `raw_length'.
      if [[ -z "${raw_segments[i + 1]}" ]]; then
        (( i++ ))
        (( tokenized_segments[zero_run_i]++ ))
      fi

      (( zero_run_i++ ))
    elif (( decimal_segment == 0 )); then
      (( tokenized_segments[zero_run_i]++ ))

      # The run is over if the next segment is not 0, so increment the
      # tracking index.
      printf -v next_decimal_segment -- '%d' "0x${raw_segments[i + 1]}"

      (( next_decimal_segment != 0 )) && (( zero_run_i++ ))
    else
      # Prefix the raw segment with `nonzero_prefix' to mark this as a
      # non-zero field.
      tokenized_segments[zero_run_i]="${nonzero_prefix}${decimal_segment}"
      (( zero_run_i++ ))
    fi
  done

  if [[ "$raw_address" == *::* ]]; then
    if (( ${#tokenized_segments[*]} == length )); then
      log_invalid_ipv6 "single '0' fields should not be compressed"
      return 1
    else
      local -i largest_run_i=0 largest_run=0

      for (( i = 0 ; i < ${#tokenized_segments[@]}; i ++ )); do
        # Skip groups that aren't zero-runs
        [[ "${tokenized_segments[i]:0:1}" == "$nonzero_prefix" ]] && continue

        if (( tokenized_segments[i] > largest_run )); then
          (( largest_run_i = i ))
          largest_run="${tokenized_segments[i]}"
        fi
      done

      local -i compressed_run="${tokenized_segments[compressed_i]}"

      if (( largest_run > compressed_run )); then
        log_invalid_ipv6 "the compressed run of all-zero fields is smaller than the largest such run"
        return 1
      elif (( largest_run == compressed_run )) && (( largest_run_i < compressed_i )); then
        log_invalid_ipv6 "only the leftmost largest run of all-zero fields should be compressed"
        return 1
      fi
    fi
  fi

  for segment in "${tokenized_segments[@]}"; do
    if [[ "${segment:0:1}" == "$nonzero_prefix" ]]; then
      printf -- '%04x\n' "${segment#${nonzero_prefix}}"
    else
      for (( n = 0 ; n < segment ; n++ )); do
        echo 0000
      done
    fi
  done
}

process_dns_ipv6() {
  local address="$1"
  shift

  info "Adding IPv6 DNS Server ${address}"

  local -a segments=()
  segments=($(parse_ipv6 "$address")) || return $?

  # Add AF_INET6 and byte count
  dns_servers+=(10 16)
  for segment in "${segments[@]}"; do
    dns_servers+=("$((16#${segment:0:2}))" "$((16#${segment:2:2}))")
  done

  (( dns_server_count += 1 ))
}

process_domain() {
  local domain="$1"
  shift

  info "Adding DNS Domain ${domain}"
  if [[ $dns_domain_count -eq 1 ]]; then
    (( dns_search_count += 1 ))
    dns_search+=("${domain}" false)
  else
    (( dns_domain_count = 1 ))
    dns_domain+=("${domain}" false)
  fi
}

process_adapter_domain_suffix() {
  # This enables support for ADAPTER_DOMAIN_SUFFIX which is a Microsoft standard
  # which works in the same way as DOMAIN to set the primary search domain on
  # this specific link.
  process_domain "$@"
}

process_domain_search() {
  local domain="$1"
  shift

  info "Adding DNS Search Domain ${domain}"
  (( dns_search_count += 1 ))
  dns_search+=("${domain}" false)
}

process_domain_route() {
  local domain="$1"
  shift

  info "Adding DNS Routed Domain ${domain}"
  (( dns_routed_count += 1 ))
  dns_routed+=("${domain}" true)
}

process_dnssec() {
  local option="$1" setting=""
  shift

  case "${option,,}" in
    yes|true)
      setting="yes" ;;
    no|false)
      setting="no" ;;
    default)
      setting="default" ;;
    allow-downgrade)
      setting="allow-downgrade" ;;
    *)
      local message="'$option' is not a valid DNSSEC option"
      emerg "${message}"
      return 1 ;;
  esac

  info "Setting DNSSEC to ${setting}"
  dns_sec="${setting}"
}

main() {
  local script_type="${1}"
  shift
  local dev="${1:-$dev}"
  shift

  if [[ -z "$script_type" ]]; then
    usage 'No script type specified'
    return 1
  elif [[ -z "$dev" ]]; then
    usage 'No device name specified'
    return 1
  elif ! declare -f "${script_type}" &>/dev/null; then
    usage "Invalid script type: '${script_type}'"
    return 1
  else
    if ! read -r link if_index _ < <(get_link_info "$dev"); then
      usage "Invalid device name: '$dev'"
      return 1
    fi

    "$script_type" "$link" "$if_index" "$@" || return 1
    # Flush the DNS cache
    systemd-resolve --flush-caches
  fi
}

if [[ "${BASH_SOURCE[0]}" == "$0" ]] || [[ "$AUTOMATED_TESTING" == 1 ]]; then
  set -o nounset

  main "${script_type:-down}" "$@"
fi
```

`/usr/lib/systemd/system/openvpn-client@.service` contents:

```
[Unit]
Description=OpenVPN tunnel for %I
After=syslog.target network-online.target
Wants=network-online.target
Documentation=man:openvpn(8)
Documentation=https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage
Documentation=https://community.openvpn.net/openvpn/wiki/HOWTO

[Service]
Type=notify
PrivateTmp=true
WorkingDirectory=/etc/openvpn/client
ExecStart=/usr/bin/openvpn --suppress-timestamps --nobind --config %i.conf
User=openvpn
Group=network
AmbientCapabilities=CAP_IPC_LOCK CAP_NET_ADMIN CAP_NET_RAW CAP_SETGID CAP_SETUID CAP_SYS_CHROOT CAP_DAC_OVERRIDE
CapabilityBoundingSet=CAP_IPC_LOCK CAP_NET_ADMIN CAP_NET_RAW CAP_SETGID CAP_SETUID CAP_SYS_CHROOT CAP_DAC_OVERRIDE
LimitNPROC=10
DeviceAllow=/dev/null rw
DeviceAllow=/dev/net/tun rw
ProtectSystem=true
ProtectHome=true
KillMode=process

[Install]
WantedBy=multi-user.target
```

`/etc/systemd/system/openvpn-client@.service.d/override.conf` contents:
 
```
[Service]
PIDFile=/run/openvpn-client/openvpn_%i.pid
ProtectHome=read-only
ExecStartPost=/bin/sleep 5
User=root
Group=root
```





