## 24-zabbix_agent

Installs and configures zabbix agent on host. Also performs the installation of additional modules. Please
note that you will need to activate each module within inventory file.

List of additional modules:

- smart. S.M.A.R.T. status detection for autodiscovered disks. In order to activate set to `yes` `zabbix_agent_data.modules.smart`.

### Inventory

```
zabbix_agent_data:
  allowed_servers: 192.168.35.14/32
  hostname: metis.xand.local
  listen_ip: 192.168.35.10
  modules:
    smart: yes
```

### Inventory secret

Does not apply.




