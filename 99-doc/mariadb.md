### 27-mariadb_host

Installs mariadb server

### Inventory

```
mariadb_host_data:
  initial_db:
    name: test_database
    encoding: utf8mb4
    collation: utf8mb4_bin
    username: test_user
```

### Inventory secrets

```
mariadb_host_data_secret:
  root_password: zzzz
  initial_user_password: kkkk
```








