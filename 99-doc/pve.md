## 01-install

Installs PVE on node from master image files.

Please note the following about inventory:
- `static_hosts` should list all members of the cluster with their local addresses. I.e., addresses provided
as `-link0` parameter when joining the cluster.
- After joining the cluster, before joining the cluster you must stop the firewal with `pve-firewall stop`
command.
- If you plan to use some a VM for routing purposes, you MUST disable checksum offloading on main interface.
You may achieve this with the following commands: `ethtool -K eth0 gso off`, `ethtool -K eth0 tso off`.

You may use the following commands to execute playbook:
```
ansible-playbook -c paramiko --ask-pass --limit sol-04 -i inventory/production 01-install/playbook.yml
ansible-playbook -c paramiko --ask-pass --limit sol-04 -i inventory/production 02-post_install/playbook.yml
```

If you need to clean the machine after unsuccessful installation you may use the following commands:
```
lvremove -y /dev/pve/swap
lvremove -y /dev/pve/data
lvremove -y /dev/pve/vz
lvremove -y /dev/pve/root
mdadm --stop /dev/md1
mdadm --stop /dev/md0
wipefs -a /dev/sdb3
wipefs -a /dev/sdb2
wipefs -a /dev/sdb1
wipefs -a /dev/sda3
wipefs -a /dev/sda2
wipefs -a /dev/sda1
wipefs -a /dev/sda
wipefs -a /dev/sdb
```

### Inventory

```
ansible_host: server-04.example.com
ansible_user: root
p_device: /dev/sda
s_device: /dev/sdb
part_prefix: ''
pve_root_size: 10G
pve_vz_size: 1024G
pve_data_size_expand: 7602900M
pve_data_tmeta_size_expand: 8000M
hostname: server-04.example.com
static_hosts:
- hostname: server-01.example.com
  ip: 10.10.10.1
- hostname: server-02.example.com
  ip: 10.10.10.2
- hostname: server-03.example.com
  ip: 10.10.10.3
tz: 'Europe/Madrid'
ip: 10.10.10.4
dns_server:
- 213.133.98.98 
- 213.133.99.99 
- 213.133.100.100 
network:
  enable_v6_loopback: yes
  interfaces:
  - name: eno1
    type: static
    address: 10.10.10.4
    gateway: 5.9.136.1
    pointopoint: 5.9.136.1
    ip_rules:
    - 'post-up ethtool -K eno1 tso off'
    - 'post-up ethtool -K eno1 gso off'
  - name: eno1.4001
    type: static
    mtu: 1400
    ip_rules: []
  - name: eno1.4091
    type: static
    address: 172.16.10.4/24
    mtu: 1400
    ip_rules: []
  - name: enp1s0
    type: static
    address: 192.168.1.104/24
    mtu: 9000
    ip_rules: []
  - name: enp1s0.21
    type: static
    address: 172.16.11.4/24
    mtu: 9000
    ip_rules: []
  - name: vmbr2
    type: static
    mtu: 1400
    ip_rules:
    - 'bridge-ports eno1.4001'
    - 'bridge-stp off'
    - 'bridge-fd 0'
reboot_after_install: yes

root_ssh_keys:
- ssh-rsa AAAAB3NzaC1yn... my@ssh
backup:
  jobs_path: '/var/lib/vz/backup/jobs'
  storage:
  - name: 'ftp_example.com'
    type: 'ftps'
    encrypt: false
    rentention_days: 7
    conn_data_root: 'ftp_root'
    conn_data_host: 'ftp.example.com'
    conn_data_port: 21
    conn_data_username: 'server04_upload'
postfix:
  rewrite_from_name: 'server-04 notification'
  rewrite_from_address: example@gmail.com
  server: smtp.gmail.com
  port: 587
  username: example@gmail.com
syncthing:
  user: syncthing
  home: /var/lib/vz/sync
  address: tcp://172.16.11.4
  listenAddress: tcp://172.16.11.4
  gui_listen: 172.16.11.4:8384
  gui_user_name: admin
zabbix:
  passive_server_address: ''
  passive_start_agents: 0
  active_server_address: 10.11.12.13
  hostname: server-04.overlap.net
reboot: yes
```

### Inventory secrets

```
root_passwd: root_secret_passwd
postfix_password: postfix_secret_passwd
syncthing_gui_user_password_bcrypt: '$2y$12$4OFCoTcBaaaaaazhO0SnXPXBeE0VcQxJ1GeQxcrP3Vu/J7oy'
storage_password:
  server04_upload:
    conn_data_password: 'secret_passwd'
```

## Join a cluster

In order to join a cluster you may issue the following command:
`pvecm add <ip_of_existing_node_on_net0> -link0 <own_ip_on_net0> -link1 <own_ip_on_net1>`

You should always use a redundant network configuration with two separate NICs if possible. More info about
PVE clusters [here](https://pve.proxmox.com/wiki/Cluster_Manager).










