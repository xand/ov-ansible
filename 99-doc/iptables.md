## 22-iptables_filter_host

This playbook configures basic iptables rules for filtering.

In addition to defined input/output rules the following is defined:

- lo interface is always ACCEPT.
- All ESTABLISHED or RELATED ctstate packets are ACCEPT in INPUT chain.
- All ESTABLISHED ctstate packets are ACCEPT in OUTPUT chain.
- All INVALID ctstate packets are DROP in INPUT chain.
- Ping is allowed in both directions

All rules are persisted, meaning that they will survive the machine reboot.

**ATTENTION**: Never disable SSH incoming rule or you will be locked out.

`masquerade_output_nat` parameter may be useful for [99-doc/openvpn.md](openvpn.md) configuration, since in generates rules like `-A POSTROUTING -s 10.66.78.0/24 ! -d 192.168.35.16/32 -o enp2s0f0 -j MASQUERADE`.

### Inventory

```
iptables_filter_host_data:
  masquerade_output_nat:
  - interface: enp2s0f0
    source: 10.66.78.0/24
    destination: '!192.168.35.16/32'
  - interface: enp2s0f0
    source: 10.66.78.0/24
    destination: '!192.168.35.14/32'
  input:
    policy: DROP
    rules:
    - protocol: tcp
      port: 22
      address: 0.0.0.0/0
      comment: 'SSH'
    - protocol: tcp
      port: 21
      address: 0.0.0.0/0
      comment: 'FTP'
    - protocol: tcp
      port: '12000:12100'
      address: 0.0.0.0/0
      comment: 'FTP Passive ports'
  output:
    policy: DROP
    rules:
    - protocol: udp
      port: 53
      address: 172.16.2.114/32
      comment: 'DNS'
    - protocol: udp
      port: 123
      address: 172.16.2.114/32
      comment: 'NTP'
```

### Inventory secrets

Does not apply.





