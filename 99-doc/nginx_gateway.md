### 21-nginx_gateway_host

Playbook which configures nginx gateway host.

This playbook configures nginx to use letsencrypt certificates. However you may want to use your own
certificates. In order to achieve this you will need to provide the certificate/privkey content in your
inventory file:

- If you want to use letsencrypt certificates just set `certbot` parameter to `yes`.
- If you want to use your own certificates you must provide `certbot: no` and then define `custom_cert`
in your inventory file. Please see examples.

Considerations:

- When specifying each `location` element you may either `upstream` or `redirect_301`.

## Inventory

```
nginx_gateway_host_data:
  certbot:
    acme_email: email@example.com
    webroot: /tmp/certbot_webroot
    use_proxy: yes
    http_proxy: http://example.com:3128
    https_proxy: http://example.com:3128
  resolvers: ['192.168.122.1']
  upstreams:
  - name: system_01_upstream
    servers:
    - address: 192.168.122.1
      port: 8086
  - name: system_02_upstream
    servers:
    - address: 192.168.122.1
      port: 8087
  domains:
  - name: x1.example.com
    certbot: no
    enabled: yes
    client_max_body_size: 1m
    locations:
    - url: /
      upstream: system_01_upstream
      headers:
      - name: Upgrade
        value: '$http_upgrade'
      - name: Connection
        value: '$connection_upgrade'
  - name: x2.example.com
    certbot: no
    enabled: yes
    client_max_body_size: 1m
    locations:
    - url: /
      upstream: system_01_upstream
      headers: []
    - url: /location_2
      upstream: system_02_upstream
      headers:
      - name: Upgrade
        value: '$http_upgrade'
      - name: Connection
        value: '$connection_upgrade'
  - name: x3.example.com
    certbot: no
    enabled: yes
    client_max_body_size: 1m
    custom_cert:
      fullchain_body: |
        -----BEGIN CERTIFICATE-----
        ...
        -----END CERTIFICATE-----
        -----BEGIN CERTIFICATE-----
        ...
        -----END CERTIFICATE-----
      chain_body: |
        -----BEGIN CERTIFICATE-----
        ...
        -----END CERTIFICATE-----
        -----BEGIN CERTIFICATE-----
        ...
        -----END CERTIFICATE-----
    locations:
    - url: /location_2
      redirect_301: https://google.com
```

## Inventory secret

```
nginx_gateway_host_data_secret:
  x3.example.com:
    custom_cert:
      priv_key_body: |
        -----BEGIN PRIVATE KEY-----
        ...
        -----END PRIVATE KEY-----
```



