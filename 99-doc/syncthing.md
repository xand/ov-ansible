## 17-syncthing_host

Syncthing host.

### Inventory

```
syncthing_host_data:
  user: sync_user
  home: /opt/sync
  gui_listen: '0.0.0.0:8384'
  gui_user_name: 'admin00'
```

### Inventory secret

```
syncthing_host_data_secret:
  gui_user_password_bcrypt: '$2b$15$6nmRptuZ3G1lFkWFjxe2H.f4lE8gS5wrViJT/PpqzBoTiJJF1TB46'
```

Notes:

- `gui_user_password_bcrypt` must be in `bcrypt` format. You may use the following snippet to generate
bcrypt-password `python -c 'import bcrypt; print(bcrypt.hashpw(b"PASSWORD", bcrypt.gensalt(rounds=15)).decode("ascii"))'`


