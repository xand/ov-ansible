### 23-ntpd_host

Playbook which configures ntpd proxy server

Consider following configuration example:

```
  children:
    ntpd_host:
      hosts:
        debian:
          ntpd_host_data:
            allowed_networks:
            - ip: 192.168.122.0
              mask: 255.255.255.0
```



