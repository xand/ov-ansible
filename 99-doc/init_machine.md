# Machine initialization

In order to the machine to be suitable for ansible management it needs to meet some requirements.

## Initial situation

A provided machine should meet the following creteria:

- Being accessible with `root` user using the password.
- No other users exist on the machine.
- Bash executable resides (maybe symlink) in `/usr/bin/bash`.
- Packages `secure-delete`, `cloud-guest-utils` must be installed.

## Requirement for ansible management

The machine must have `python 3` installed in the following path: `/usr/bin/python3`. Otherwise a warning
message will be shown and deprecation may occur at some point in time.

### SSH configuration

Basically a copy of `sshd_config` file will be performed setting up following features:

- [PENDING] Login for root must be disabled. Actually the login is allowed w/o password.
- All comments and whitelines from SSH configuration file are removed.
- SSH session is closed after 5 minutes of inactivity. `ClientAliveInterval 300` and 
`ClientAliveCountMax 0`.
- SSH host keys are regenerated.
- [PENDING] `AllowUsers` must be configured.
- `PermitEmptyPasswords no`.
- `ListenAddress` is configured via `network.ssh.listen_address` directive.
- Set `IgnoreRhosts yes` directive.
- Set `HostbasedAuthentication no` directive.
- `KexAlgorithms ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha256`
- `Ciphers aes128-ctr,aes192-ctr,aes256-ctr`
- `MACs hmac-sha2-256,hmac-sha2-512,hmac-sha1`
- `UseDNS no`
- `X11Forwarding no`
- `LogLevel VERBOSE`
- `HostKeyAlgorithms ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa,ssh-dss`
- `AllowTcpForwarding no`
- `AllowStreamLocalForwarding no`
- `GatewayPorts no`
- `PermitTunnel no`

Systemd ssh.service service is configured in such a way that when it starts it checks that host keys are
present, if not, automatically regenerates them. Check `/etc/systemd/system/ssh.service.d/override.conf` file
for details:

```
[Service]
ExecStartPre=/usr/bin/bash -c '/usr/bin/test -f /etc/ssh/ssh_host_rsa_key || /usr/sbin/dpkg-reconfigure openssh-server'
```

`authorized_keys` file is located under `/etc/ssh/authorized_keys/$USER` path.


### Root user modifications

If you need to change root password you should make use of the following procedure:

1. Create a regular file `/root/.change_password`. This file will contain plain password for root user.
2. Reboot the machine. The password will be set on next boot.

### Management users

- File `~/.ssh/authorized_keys` is removed for all users. In order to allow user to login
the keys must exist in `/etc/ssh/authorized_keys/$USER` file. This way the user is not able
to add arbitrary keys.

- All users in `sudo` group are allowed to execute any command as root without password prompt.

### Setup machine network

### Inventory

```
network:
  mac: 'B8:27:EB:5A:91:81'
  dhcp: no
  address: 192.168.35.14
  netmask: 255.255.255.0
  gateway: 192.168.35.1
  dns: ['192.168.35.1']
  pointopoint:
  mtu: 
  routes: []
hostname: alix
resize_fs: no
resize_fs_device: /dev/mmcblk0
resize_fs_partition: '2'
apt_set_proxy: no
apt_http_proxy: 
apt_https_proxy: 
ssh:
  listen_address: 192.168.35.14
```

### Inventory secret

```
root_pubkeys:
  - ssh-rsa AAAAB3Nza... comment@machine

root_passwd: secret_root_passwd

```


