---
- hosts: all
  gather_facts: true
  tasks:
  - name: Include secret vars
    include_vars:
      file: '{{ lookup("fileglob", "{{ item }}/host_vars/{{ inventory_hostname }}_secret.yml") }}'
    with_items: '{{ ansible_inventory_sources }}'

  - name: Install required packages
    apt:
      name: ['screen', 'mlocate', 'pigz', 'ifupdown2', 'ethtool', 'sudo']
      update_cache: yes

  - name: Create locate database
    shell:
      cmd: updatedb

# ------------------------------------------------------------------
# Add ssh keys
# ------------------------------------------------------------------
  - name: Set root authorized keys
    template:
      src: templates/authorized_keys.j2
      dest: /root/.ssh/authorized_keys
      mode: 0600
      owner: root
      group: root

# ------------------------------------------------------------------
# Configure SSH
# ------------------------------------------------------------------
  - name: Set SSH configuration
    copy:
      src: files/sshd_config
      dest: /etc/ssh/sshd_config
      owner: root
      group: root
      mode: 0644

# ------------------------------------------------------------------
# Set swappiness to 1
# ------------------------------------------------------------------
  - name: Set vm.swappiness to 1
    sysctl:
      name: vm.swappiness
      value: 1

# ------------------------------------------------------------------
# Configure postfix
# ------------------------------------------------------------------
  - name: Install libsasl2-modules 
    apt:
      name: libsasl2-modules 

  - name: Template configuration files
    template:
      src: '{{ item.src }}'
      dest: '{{ item.dst }}'
    with_items:
    - { src: templates/header_check.j2, dst: /etc/postfix/header_check }
    - { src: templates/main.cf.j2, dst: /etc/postfix/main.cf }
    - { src: templates/sender_canonical_maps.j2, dst: /etc/postfix/sender_canonical_maps }

  - name: Template configuration files
    template:
      src: '{{ item.src }}'
      dest: '{{ item.dst }}'
      mode: 0600
    with_items:
    - { src: templates/sasl_passwd.j2, dst: /etc/postfix/sasl/sasl_passwd }

  - name: Generate password db
    shell:
      cmd: postmap /etc/postfix/sasl/sasl_passwd

  - name: Restart postfix
    systemd:
      name: postfix
      state: restarted

# ------------------------------------------------------------------
# Configure syncthing
# ------------------------------------------------------------------
  - name: Add apt key for syncthing
    apt_key:
      url: https://syncthing.net/release-key.txt

  - name: Add apt channel to sources
    apt_repository:
      repo: deb https://apt.syncthing.net/ syncthing stable

  - name: Install syncthing
    apt:
      name: ['syncthing', 'python-lxml', 'python3-lxml']

  - name: Create user if needed
    user:
      name: '{{ syncthing.user }}'
      password: '!'
      state: present
      update_password: on_create
      home: '{{ syncthing.home }}'
      shell: /bin/false
    register: user

  - name: Enable and start syncthing as user for creating configuration
    systemd:
      name: 'syncthing@{{ syncthing.user }}.service'
      enabled: yes
      state: started

  - name: Stop syncthing
    systemd:
      name: 'syncthing@{{ syncthing.user }}.service'
      state: stopped

  - name: Set web gui on all interfaces
    xml:
      path: '{{ user.home }}/.config/syncthing/config.xml'
      xpath: '{{ item.xpath }}'
      value: '{{ item.value }}'
    with_items:
    - { xpath: /configuration/gui/address, value: '{{ syncthing.gui_listen }}' }
    - { xpath: /configuration/gui/password, value: '{{ syncthing_gui_user_password_bcrypt }}' }
    - { xpath: /configuration/gui/user, value: '{{ syncthing.gui_user_name }}' }
    - { xpath: /configuration/options/listenAddress, value: '{{ syncthing.listenAddress }}' }

  - name: Start syncthing
    systemd:
      name: 'syncthing@{{ syncthing.user }}.service'
      state: started 

  - name: Create .stfolder in dump directory
    file:
      path: /var/lib/vz/dump/.stfolder
      state: directory
      owner: '{{ syncthing.user }}'

  - name: Create .stignore in dump directory
    copy:
      dest: /var/lib/vz/dump/.stignore
      content: |
        *.tmp
        *.dat
      owner: '{{ syncthing.user }}'

# ------------------------------------------------------------------
# Install zabbix
# ------------------------------------------------------------------
  - name: Copy deb package
    copy:
      src: files/zabbix-release_5.0-1+buster_all.deb
      dest: /tmp/zabbix.deb
      owner: root
      group: root
      mode: 0755

  - name: Install deb package for zabbix
    apt:
      deb: /tmp/zabbix.deb
      update_cache: yes

  - name: Install zabbix-agent
    apt:
      name: zabbix-agent2
      update_cache: yes

  - name: Create zabbix directories
    file:
      path: '{{ item }}'
      state: directory
      owner: root
      group: root
      mode: 0755
    with_items:
    - /etc/zabbix/scripts

  - name: Cleanup installation file
    file:
      path: /tmp/zabbix.deb
      state: absent

  - name: Template configuration file
    template:
      src: templates/zabbix_agent2.conf.j2
      dest: /etc/zabbix/zabbix_agent2.conf
      owner: root
      group: root
      mode: 0644

  - name: Import smart module (Install)
    import_tasks: smart_module_install.yml
    when: zabbix_agent_data.modules.smart is defined and zabbix_agent_data.modules.smart

  - name: Import smart module (Remove)
    import_tasks: smart_module_remove.yml
    when: zabbix_agent_data.modules.smart is not defined or not zabbix_agent_data.modules.smart

  - name: Enable and start zabbix agent
    systemd:
      name: zabbix-agent2
      state: restarted
      enabled: yes

# ------------------------------------------------------------------
# Configure vzdump
# ------------------------------------------------------------------
  - name: Template vzdump.conf file
    template:
      src: templates/vzdump.conf.j2
      dest: /etc/vzdump.conf
      owner: root
      group: root
      mode: 0644

# ------------------------------------------------------------------
# Reboot system
# ------------------------------------------------------------------
  - name: Reboot
    shell:
      cmd: systemctl reboot
    ignore_errors: yes
    when: reboot


