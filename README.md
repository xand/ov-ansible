# ov-ansible

## Inventories

Inventories should be treated as submodules. Cloning should occur like following:

1. From the gitlab control panel fork the project.
2. Clone forked project: `git clone git@overlap-gitlab:Overlap/ov-ansible.git`
3. Inside the cloned project add inventory submodule:
```
cd ov-ansible
git submodule add git@overlap-gitlab:Overlap/ov-ansible-inventory.git inventory
```
4. Commit submodule configuration as usual:
```
git add .
git commit -m 'Add submodule'
git push -u origin master
```

Please note, when working with submodules you will need to commit twice. First time the submodule with
the modifications, after that, the main project you are working on. More info at 
[git official site](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

This operation is needed only once when creating forked project for the first time. Later, when you need
to clone the project don't forget to update submodules also with `--recurse-submodules` to `git clone` command.

Read more about [inventories](99-doc/inventories.md).

You should periodically update your forked project with upstream changes. You may proceed as follows:

1. Add main upstream repository to your forked project:
```
git remote add main-upstream https://gitlab.com/xand/ov-ansible.git
```
2. When you desire to sync with main upstream repository perform the pull:
```
git pull main-upstream master
```
3. Perform push to your origin repository:
```
git push -u origin master
```

It's highly recommended to keep your project in sync with the upstream. You can achieve this with
fork [Mirror Repository](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/) feature offered by Gitlab. Also more information [here](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html).

## Preparing the environment for new customer

This section describes the necessary steps for creating an environment for a new customer.

1. Create new entry in `~/.ansible.cfg` and vault's password script as described 
[here](99-doc/vault.md).

2. Create a new directory for your customer. For example, let's say the name is `customer2`,
so, you will need to create something like that:
    - `~/projects/customer2/ansible-inventory/setup/` -- directory for keeping the initial 
    setup for the machines.
        - `~/projects/customer2/ansible-inventory/setup/hosts` -- hosts for setup
        - `~/projects/customer2/ansible-inventory/setup/host_vars/` -- directory for keeping
        host variables for setup.
    - `~/projects/customer2/ansible-inventory/production/` -- directory for keeping
    production setup for the machines.
        - `~/projects/customer2/ansible-inventory/production/hosts` -- hosts for production. 
        - `~/projects/customer2/ansible-inventory/production/host_vars/` -- directory for
        keeping host variables for production.

## Adding new machine

This section describes how to add new machine under ansible control.

First of all you must already have your machine up and running meeting the following
conditions:

- Publicly available IP address.
- SSH daemon up, running and accessible.
- `root` access.
- A valid server name must be defined beforehand.

For the example, let's say we are going to create a server named `rundeck` for the customer
`customer2`. Let's say the ip address is `8.8.8.8`. If the instance meets the 
requirements you may perform the following steps to add new machine:

1. Create `~/projects/customer2/ansible-inventory/setup/host_vars/rundeck.yml`.
Put the following content inside:
```
network:
    network_configure: no
        network_content: |
            source /etc/network/interfaces.d/*

            auto lo
            iface lo inet loopback
        resolv_configure: no
            resolv_content: |
                nameserver 185.12.64.1
                nameserver 185.12.64.2
hostname: tradler_rundeck
resize_fs: no
apt_set_proxy: no
```

2. Create `~/projects/customer2/ansible-inventory/setup/host_vars/rundeck_secret.yml`.
See [vault.md](99-doc/vault.md). Put the following content inside:
```
root_pubkeys:
  - ssh-rsa AAAAB3NzaC1yc [... cut for brevity ...] QTBLnQ== zzz
root_passwd: MyVerySecretRootPassword
```

3. Update `~/projects/customer2/ansible-inventory/setup/hosts` with the following content:
```
rundeck ansible_ssh_host=8.8.8.8 ansible_ssh_user=root
```

4. Execute the setup playbook:
```
$ ansible-playbook -i ~/projects/customer2/ansible-inventory/setup/hosts setup.yml
```

## 00-setup

This directory contains basic tasks for setting up machine ready for playbooks.

The steps to setup a machine are the following:

1. Prepare the `hosts` file.

Here is an example:

```
all:
  hosts:
    freshly_install_machine_01:
      ansible_ssh_host: 192.168.122.11
      ansible_ssh_user: root 
      network:
        mac: '52:54:00:f7:93:1b'
        dhcp: false
        address: 192.168.122.12
        netmask: 255.255.255.0
        gateway: 192.168.122.1
        dns: 192.168.122.1
        pointopoint:
        mtu:
        routes:
        - net: 10.90.0.0/16
          via: 172.16.2.101
          dev: '52:54:00:f7:93:1b'
        network_configure: no
        network_content: |
          source /etc/network/interfaces.d/*

          # The loopback network interface
          auto lo
          iface lo inet loopback

          auto enp1s0
          iface enp1s0 inet static
              address 192.168.122.13
              netmask 255.255.255.0
              gateway 192.168.122.1
        resolv_configure: no
        resolv_content: |
          nameserver 192.168.122.1
      hostname: debian
      local_pubkeys_directory: ~/.secret-data/ansible/local_test/pubkeys
      resize_fs: true
      resize_fs_device: /dev/vda
      resize_fs_partition: 1
      root_passwd: MyVerySecretPasswd
      local_sources_list: ~/.secret-data/ansible/setup/data/sources.list
      apt_set_proxy: false
      apt_http_proxy: 'http://localhost:3128'
      apt_https_proxy: 'http://localhost:3128'
```

Notes:
- If `network_configure` is `no` than no configuration is performed. Just the contents of `network_content` is copied inside `/etc/network/interfaces`. Otherwise, full configuration is performed.
- If `resolv_configure` is `no` than no configuration is performed. Just the contents of `resolv_content` is copied inside `/etc/resolv.conf`. Otherwise, full configuration is performed.


2. Execute the playbook with the following command: `ansible-playbook -c paramiko -i ~/.secret-data/ansible/setup/inventory --ask-pass setup.yml`

For troubleshooting you may use the following env. variables:
```
ANSIBLE_HOST_KEY_CHECKING=False
ANSIBLE_DEBUG=False
```

3. Manually login into the machine and reboot it in order to apply changes.

You may clone the VM executing something like this:
```
sudo virt-clone -o debian10-clean --auto-clone -n debian10-setup -m '52:54:00:9d:f1:22'
sudo virsh start debian10-setup
```

## 01-playbooks

This directory contains main playbooks.

**Ansible disable switch**. In order to prevent accidental execution each host defines custom fact `allow_ansible`. This switch is set to false (disabling further ansible run) at the end of each execution. If you want to enable it again execute `echo -n "true" > /etc/ansible/facts.d/allow_ansible.fact` on the host.

### 01-docker_host

See: [docker.md](99-doc/docker.md).

### 02-pg11-database

See: [postgres.md](99-doc/postgres.md).

### 003-sftp-pod-host

Prepares the host for hosting SFTP pods. This playbook expects that docker is already installed on host machine. Please note that this playbook will use [panubo/sshd](https://hub.docker.com/r/panubo/sshd/) docker image.

This pod offers 3 mutually exclusive modes:
- sftp mode
- rsync mode
- scp mode

Configuration example:
```
  children:
    sftp_pod_host:
      hosts:
        debian:
          files_owner_group: sftp_pod # this group will be created and all users specified by files_owner_name/uid will be assigned to this group
          pods:
            - {
              name: johnsnow_backup, # name for the created pod
              port: 48002, # listen port
              storage_root_path: /opt/johnsnow_backup, # root path to store everything
              ssh_pubkeys_path: /home/xand/.secret-data/ansible/local_test/data/sftp_pod_host/authorized_keys, # this path must be the host where the ansible playbook is being executed
              files_owner_name: sftp_pod_jsb, # this user will be created
              docker_ssh_enable_root_param: 'false', # see: https://github.com/panubo/docker-sshd 
              docker_sftp_mode_param: 'true', # see: https://github.com/panubo/docker-sshd 
              docker_sftp_chroot_param: 'true', # see: https://github.com/panubo/docker-sshd 
              docker_rsync_param: 'false'  # see: https://github.com/panubo/docker-sshd 
            }
```

**Please note simple quotes around some parameters, they are really necessary.**

### 04-luks_pod_host

See: [luks.md](99-doc/luks.md).

### 06-vsftpd_pod_host

See: [vsftpd.md](99-doc/vsftpd.md).

### 07-setup

See: [setup.md](99-doc/setup.md).

### 09-afraid.org_manager

See: [afraid.md](99-doc/afraid.md).

### 10-jdk_11_host

Install JDK 11 on the host

Configuration example
```
  children:
    jdk_11_host:
      hosts:
        debian:
          jdk_11_host_data:
            install_path: /opt/software/jdk-11.0.2
            archive_path_src: /home/xand/.secret-data/ansible/local_test/data/jdk11_host/jdk-11.0.2.zip
```

### 11-keycloak_10_host

See: [keycloak.md](99-doc/keycloak.md)

### 12-barman_host

See: [barman.md](99-doc/barman.md)

### 13-net_gateway_host

Creates machine for acting as gateway

Please note: when mapping a port range it should be identical on both sides, for example: 12000:12100. Also
it must be separated by colon.

```
  children:
    net_gateway_host:
      hosts:
        debian:
          net_gateway_host_data:
            enable_fw_dropped_log: yes
            route_tables:
            - via: 5.9.139.45
              fwmark: 126 # this will be used for the table name as well
              dev: 'E2:DF:C5:3A:5A:87'
            mappings:
            - ip: 5.9.237.126
              port: 80
              dest_ip: 172.16.2.98
              dest_port: 80
              protocol: tcp
              mark: 126
            - ip: 5.9.237.126
              port: 443
              dest_ip: 172.16.2.98
              dest_port: 443
              protocol: tcp
              mark: 126
            - ip: 5.9.237.126
              port: '12000:12100'
              dest_ip: 172.16.2.98
              dest_port: '12000:12100'
              protocol: tcp
              mark: 126
            gateway_local_network: '172.16.2.0/24' 
            gateway:
            - ip: 172.16.2.3
              port: 80
              protocol: tcp
```

** Notes:

- `enable_fw_dropped_log`. This option may help in case you need to debug dropped packets in forward chain. 
For performance reasons should be disabled all the time.

### 14-squid_host

Squid caching proxy server

```
  children:
    squid_host:
      hosts:
        debian:
          squid_host_data:
            safe_ports: [80, 443]
            ssl_ports: [443]
            whitelist_sites:
            - '.google.ie'
            - '.aport.ru'
            whitelist_hosts:
            - '192.168.122.0/24'
```

### 15-dnsmasq_host

dnsmasq host

```
  children:
    dnsmasq_host:
      hosts:
        debian:
          dnsmasq_host_data:
            listen_address: 192.168.35.16
            dns: ['8.8.8.8', '8.8.4.4']
            hosts:
            - hostname: 'ariel.corp.overlap.net'
              ip: 127.0.0.1
```

### 16-zabbix_proxy_host

See: [zabbix-proxy.md](99-doc/zabbix-proxy.md).

### 17-syncthing_host

See: [syntching.md](99-doc/syncthing.md).

### 18-postfix_host

See: [postfix.md](99-doc/postfix.md).

### 19-openvpn_host

See: [openvpn.md](99-doc/openvpn.md).

### 20-zabbix_host

See: [zabbix.md](99-doc/zabbix.md).

### 21-nginx_gateway_host

See: [nginx_gateway.md](99-doc/nginx_gateway.md).

### 22-iptables_filter_host

See: [iptables.md](99-doc/iptables.md).

### 23-ntpd_host

See: [ntpd.md](99-doc/ntpd.md).

### 25-samba_share_host

See: [samba.md](99-doc/samba.md).

### 27-mariadb_host

See: [mariadb.md](99-doc/mariadb.md).

## 03-custom-software

Playbook to deploy custom software.

### 01-spring-boot-app

Deploys spring boot application.

Example configuration:

```
    spring_boot_apps:
      hosts:
        debian:
          spring_boot_apps_data:
          - name: j_chart_renderer_staging
            local_path: /home/xand/.secret-data/ansible/local_test/custom_apps/j_chart_renderer
            root: /opt/spring-boot/j_chart_renderer_staging
            user: app_j_chart_renderer_staging
            group: app_j_chart_renderer_staging
            execute: yes
            java_home: /opt/jdk-11.0.2
            environment:
            - name: JAVA_HOME
              value: /opt/jdk-11.0.2
            - name: JAVA_OPTS
              value: -Xmx1024M
```

