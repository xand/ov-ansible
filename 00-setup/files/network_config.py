#!/usr/bin/python3

import json
import subprocess

if __name__=='__main__':
    network_config = None
    with open('/tmp/network.txt', 'r') as a_file:
        network_config = json.load(a_file)

    # get interface name by mac address
    name = subprocess.check_output(['/tmp/get_net_by_mac.sh', network_config['mac'].strip()], universal_newlines=True, shell=True)
    name = name.strip()

    result_content = ''

    result_content += 'auto ' + name + '\n'

    if network_config['dhcp']:
        result_content += 'iface ' + name + ' inet dhcp\n'
        result_content += '\n'
    else:
        result_content += 'iface ' + name + ' inet static\n'
        result_content += '    address ' + network_config['address'] + '\n'
        result_content += '    netmask ' + network_config['netmask'] + '\n'

        if network_config['gateway']:
            result_content += '    gateway ' + network_config['gateway'] + '\n'

        if network_config['pointopoint']:
            result_content += '    pointopoint ' + network_config['pointopoint'] + '\n'
    
        if network_config['mtu']:
            result_content += '    mtu ' + str(network_config['mtu']) + '\n'
        
        result_content += '\n'

        for r in network_config['routes']:
            result_content += '    post-up ip route add ' + r['net'] + ' via ' + r['via']
            if r['dev']:
                dev_name = subprocess.check_output(['/tmp/get_net_by_mac.sh', r['dev']], universal_newlines=True)
                dev_name = name.strip()

                result_content += ' dev ' + dev_name + '\n'

            result_content += '    pre-down ip route del ' + r['net'] + ' via ' + r['via']
            if r['dev']:
                dev_name = subprocess.check_output(['/tmp/get_net_by_mac.sh', r['dev']], universal_newlines=True)
                dev_name = name.strip()
                result_content += ' dev ' + dev_name + '\n'

            result_content += '\n'

    # write result to file
    with open('/etc/network/interfaces.d/' + name + '.conf', 'w') as w_file:
        w_file.write(result_content)



